//
//  ViewController.swift
//  DepthDataCapture
//
//  Created by SIRT Centre on 2018-02-23.
//  Copyright © 2018 SIRT Centre. All rights reserved.
//

import UIKit
import AVFoundation
import CoreVideo
import Photos
import MobileCoreServices
import XestoCoreSwift

class ViewController: UIViewController, AVCaptureDepthDataOutputDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureDataOutputSynchronizerDelegate, UITextFieldDelegate, XestoPoseDelegate, XestoGestureDelegate {
    
    @IBOutlet weak var palmIndicatorView: UIView!
    @IBOutlet weak var thumbIndicatorView: UIView!
    @IBOutlet weak var indexIndicatorView: UIView!
    @IBOutlet weak var middleIndicatorView: UIView!
    @IBOutlet weak var ringIndicatorView: UIView!
    @IBOutlet weak var pinkyIndicatorView: UIView!
    
    var indicatorViews = [DotView]()
    
    private let xestoFramework = XestoCoreSwift();
    
    //URL for documents directory. Used as the base path for saving frames as it is
    let docDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    
    //Default file name prefix
    var filePrefix = "DepthCapture"
    
    //Index of depth/video frame. Identifies matching pairs. Incremented at each capture. Should be reset to 0 for each capture session.
    var frameIndex = 0
    
    //If true, causes all saved frames to have a timestamp in seconds appended to the end of their file name. Prevents overwriting of data.
    var appendTimestamp = true
    @IBOutlet weak var timestampSwitch: UISwitch!
    
    private let session = AVCaptureSession()
    private let depthDataOutput = AVCaptureDepthDataOutput()
    
    private let videoDataOutput = AVCaptureVideoDataOutput()
    
    private var outputSynchronizer: AVCaptureDataOutputSynchronizer?
    
    // Communicate with the session and other session objects on this queue.
    private let sessionQueue = DispatchQueue(label: "session queue", attributes: [], autoreleaseFrequency: .workItem)
    
    private enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    private var setupResult: SessionSetupResult = .success
    
    private var videoDeviceInput: AVCaptureDeviceInput!
    private let videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera,
                                                                                             .builtInTrueDepthCamera],
                                                                               mediaType: .video,
                                                                               position: .unspecified)
    private let dataOutputQueue = DispatchQueue(label: "video data queue", qos: .userInitiated, attributes: [], autoreleaseFrequency: .workItem)
    
    private let xestoOutputQueue = DispatchQueue(label: "gesture detection delegate queue", qos: .userInitiated, attributes: [], autoreleaseFrequency: .workItem)
    
    private var depthVisualizationEnabled = true
    
    private let videoDepthConverter = DepthToGrayscaleConverter()

    @IBOutlet weak var PreviewView: UIView!
    var previewLayer:AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var captureToggle: UISwitch!
    private var captureEnabled = false
    
    @IBOutlet weak var smoothingSwitch: UISwitch!
    private var smoothingEnabled = false
    
    @IBOutlet weak var filePrefixEntry: UITextField!
    
    @IBAction func onPrefixEntered(_ sender: UITextField) {
        if filePrefixEntry.text != nil{
            filePrefix = filePrefixEntry.text!
        }
        
    }
    
    @IBAction func OnTimestampToggled(_ sender: UISwitch) {
        appendTimestamp = timestampSwitch.isOn
    }
    
    @IBAction func onSmoothingToggled(_ sender: UISwitch) {
        let smoothingEnabled = sender.isOn
        
        sessionQueue.async {
            self.depthDataOutput.isFilteringEnabled = smoothingEnabled
        }
    }
    
    @IBAction func OnCaptureToggled(_ sender: UISwitch) {
        self.captureEnabled = captureToggle.isOn
        if captureToggle.isOn{
            //Reset frame index.
            frameIndex = 0
            //Disable file name entry
            filePrefixEntry.isEnabled = false
            
            //Tell the library to dump out raw depth data as well
            var curFilePrefix = "";
            if(filePrefixEntry.text != nil){
                curFilePrefix = filePrefixEntry.text!
            }
            xestoFramework.startDepthRecording(filePrefix: curFilePrefix)
        }
        else{
            filePrefixEntry.isEnabled = true
            xestoFramework.stopDepthRecording()
        }
    }
    
    
    @IBAction func SwitchCamera(_ sender: Any) {
        sessionQueue.async {
            let currentVideoDevice = self.videoDeviceInput.device
            
            self.dataOutputQueue.sync {
                self.videoDepthConverter.reset()
            }
            
            var preferredPosition = AVCaptureDevice.Position.unspecified
            switch currentVideoDevice.position {
            case .unspecified, .front:
                preferredPosition = .back
                
            case .back:
                preferredPosition = .front
            }
            
            let devices = self.videoDeviceDiscoverySession.devices
            if let videoDevice = devices.first(where: { $0.position == preferredPosition }) {
                var videoInput: AVCaptureDeviceInput
                do {
                    videoInput = try AVCaptureDeviceInput(device: videoDevice)
                } catch {
                    print("Could not create video device input: \(error)")
                    return
                }
                self.session.beginConfiguration()
                
                // Remove the existing device input first, since using the front and back camera simultaneously is not supported.
                self.session.removeInput(self.videoDeviceInput)
                
                if self.session.canAddInput(videoInput) {
                    self.session.addInput(videoInput)
                    self.videoDeviceInput = videoInput
                } else {
                    print("Could not add video device input to the session")
                    self.session.addInput(self.videoDeviceInput)
                }
                
                self.depthDataOutput.connection(with: .depthData)!.isEnabled = true
                
                //Set video and depth outputs to landscape mode
                guard let videoConnection = self.videoDataOutput.connection(with: AVFoundation.AVMediaType.video) else { return }
                guard videoConnection.isVideoOrientationSupported else { return }
                videoConnection.videoOrientation = .landscapeRight
                
                guard let depthConnection = self.depthDataOutput.connection(with: AVFoundation.AVMediaType.depthData) else { return }
                guard depthConnection.isVideoOrientationSupported else { return }
                depthConnection.videoOrientation = .landscapeRight
                
                guard depthConnection.isVideoMirroringSupported else { return }
                guard videoConnection.isVideoMirroringSupported else { return }
                
                depthConnection.automaticallyAdjustsVideoMirroring = false
                videoConnection.automaticallyAdjustsVideoMirroring = false
                
                //Set preview orientation
                if preferredPosition == .front{
                    depthConnection.isVideoMirrored = true
                    videoConnection.isVideoMirrored = true
                }
                else{
                    depthConnection.isVideoMirrored = false
                    videoConnection.isVideoMirrored = false
                }
                
                //Set depth format
                self.setCameraDepthFormat(videoDevice: videoDevice)

                self.session.commitConfiguration()
                
            }
            
        }
    }
        
    func handPoseOutput(pose: HandPose) {
        print("Got hand pose: \(pose)")
        DispatchQueue.main.async {
            self.palmIndicatorView.center = self.projectPoseOnScreen(pose: pose.palmPose);
            self.thumbIndicatorView.center = self.projectPoseOnScreen(pose: pose.fingerPoses.0);
            self.indexIndicatorView.center = self.projectPoseOnScreen(pose: pose.fingerPoses.1);
            self.middleIndicatorView.center = self.projectPoseOnScreen(pose: pose.fingerPoses.2);
            self.ringIndicatorView.center = self.projectPoseOnScreen(pose: pose.fingerPoses.3);
            self.pinkyIndicatorView.center = self.projectPoseOnScreen(pose: pose.fingerPoses.4);
        }
    }
    
    func gestureObservationOutput(gestureObservation: GestureObservation) {
        //print("Gesture observed with ID: \(gestureObservation.gestureID)")
    }
    
    //Get rid of that damn keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Set output delegates for Xesto library
        xestoFramework.setPoseDelegate(delegate: self, queue: xestoOutputQueue)
        xestoFramework.setGestureDelegate(delegate: self, queue: xestoOutputQueue)
        
        //TESTING
        //xestoFramework.startDepthRecording(filePrefix: "DepthBuffer");
        
        //UI setup
        timestampSwitch.isOn = appendTimestamp
        smoothingSwitch.isOn = smoothingEnabled
        captureToggle.isOn = captureEnabled
        filePrefixEntry.text = filePrefix
        filePrefixEntry.delegate = self
        
        // Check video authorization status, video access is required
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            // The user has previously granted access to the camera
            break
            
        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant video access
             We suspend the session queue to delay session setup until the access request has completed
             */
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if !granted {
                    self.setupResult = .notAuthorized
                }
                self.sessionQueue.resume()
            })
            
        default:
            // The user has previously denied access
            setupResult = .notAuthorized
        }
        
        sessionQueue.async {
            self.setupSession() //startRunning() is a blocking call so should be done on its own thread
        }
        
        //Setup preview video
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
        self.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        self.previewLayer.zPosition = .leastNormalMagnitude
        let rootLayer :CALayer = self.PreviewView.layer
        rootLayer.masksToBounds = true
        self.previewLayer.frame = self.view.bounds
        rootLayer.addSublayer(self.previewLayer)
        
        //Add more views
        
        
        for index in 1...1000 {
            let randomIntX = Int(arc4random_uniform(1000))
            let randomIntY = Int(arc4random_uniform(1000))
            let rect = CGRect(x: randomIntX, y: randomIntY, width: 3, height: 3)
            let singleDotView = DotView(frame: rect)
            singleDotView.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 1)
            view.addSubview(singleDotView)
            indicatorViews.append(singleDotView)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Setting up the AVCaptureSession. Should be done in its own thread
    func setupSession(){
        if setupResult != .success {
            return
        }
        
        let defaultVideoDevice: AVCaptureDevice? = videoDeviceDiscoverySession.devices.first
        
        guard let videoDevice = defaultVideoDevice else {
            print("Could not find any video device")
            setupResult = .configurationFailed
            return
        }
        
        do {
            videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
        } catch {
            print("Could not create video device input: \(error)")
            setupResult = .configurationFailed
            return
        }
        
        session.beginConfiguration()
        session.sessionPreset = .photo
        
        // Add a video input
        guard session.canAddInput(videoDeviceInput) else {
            print("Could not add video device input to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        session.addInput(videoDeviceInput)
        
        // Add a video data output
        if session.canAddOutput(videoDataOutput) {
            session.addOutput(videoDataOutput)
            videoDataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_32BGRA)]
            videoDataOutput.setSampleBufferDelegate(self, queue: dataOutputQueue)
        } else {
            print("Could not add video data output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        // Add a depth data output
        if session.canAddOutput(depthDataOutput) {
            session.addOutput(depthDataOutput)
            depthDataOutput.setDelegate(self, callbackQueue: dataOutputQueue)
            depthDataOutput.isFilteringEnabled = smoothingEnabled
            if let connection = depthDataOutput.connection(with: .depthData) {
                connection.isEnabled = depthVisualizationEnabled
            } else {
                print("No AVCaptureConnection")
            }
        } else {
            print("Could not add depth data output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        if depthVisualizationEnabled {
            // Use an AVCaptureDataOutputSynchronizer to synchronize the video data and depth data outputs.
            // The first output in the dataOutputs array, in this case the AVCaptureDepthDataOutput, is the "master" output.
            outputSynchronizer = AVCaptureDataOutputSynchronizer(dataOutputs: [depthDataOutput, videoDataOutput])
            outputSynchronizer!.setDelegate(self, queue: dataOutputQueue)
        } else {
            outputSynchronizer = nil
        }
        /*
        //Set video and depth outputs to portrait mode
        guard let videoConnection = videoDataOutput.connection(with: AVFoundation.AVMediaType.video) else { return }
        guard videoConnection.isVideoOrientationSupported else { return }
        videoConnection.videoOrientation = .portrait
        
        guard let depthConnection = depthDataOutput.connection(with: AVFoundation.AVMediaType.depthData) else { return }
        guard depthConnection.isVideoOrientationSupported else { return }
        depthConnection.videoOrientation = .portrait
         */
        
        //Set preview orientation
        let conn = self.previewLayer.connection!
        if (conn.isVideoOrientationSupported){
            conn.videoOrientation = .landscapeRight
        }/*
        if (conn.isVideoMirroringSupported){
            conn.automaticallyAdjustsVideoMirroring = false
            conn.isVideoMirrored = false
        }*/
        
        //Set depth format
        self.setCameraDepthFormat(videoDevice: videoDevice)
        
        session.commitConfiguration()
        
        self.session.startRunning()
        
    }
    
    
    // MARK: - Depth Data Output Delegate
    func depthDataOutput(_ depthDataOutput: AVCaptureDepthDataOutput, didOutput depthData: AVDepthData, timestamp: CMTime, connection: AVCaptureConnection) {
        
        CVPixelBufferLockBaseAddress(depthData.depthDataMap, .readOnly)
        
        let dataType = str4(n: depthData.depthDataType)
        let dataSize = CVPixelBufferGetDataSize(depthData.depthDataMap)
        let bytesPerRow = CVPixelBufferGetBytesPerRow(depthData.depthDataMap)
        let address = CVPixelBufferGetBaseAddress(depthData.depthDataMap)!
        
        //Note: Assumed to be hdis (default). UInt16 is used as Swift has no Float16
        let buffer = unsafeBitCast(address, to: UnsafeMutablePointer<UInt16>.self)
        
        print("[\(timestamp.seconds)] Depth data received! Type: \(dataType), Size: \(dataSize), BPR: \(bytesPerRow), Address: \(address)")
        
        /*
        let px1 = buffer[0]
        let px2 = buffer[17 * bytesPerRow + 43]
        let px3 = buffer[dataSize / 2 - 1]
        
        print("First px: \(px1), px[43, 17]: \(px2), last px: \(px3)")
        */
        
        /*
        print("=== Outputing pixels! ===")
        for px in 0..<dataSize/2{
            print("Pixel #\(px): \(buffer[px])")
        }
        print("=== Pixel output complete ===")
         */
 
        CVPixelBufferUnlockBaseAddress(depthData.depthDataMap, .readOnly)
        processDepth(depthData: depthData)
        
        /*CVPixelBufferLockBaseAddress(depthData.depthDataMap, .readOnly)
         let address = CVPixelBufferGetBaseAddress(depthData.depthDataMap)!
         let bytesPerRow = CVPixelBufferGetBytesPerRow(depthData.depthDataMap)
         let buffer = unsafeBitCast(address, to: UnsafeMutablePointer<Float32>.self)
         let px = buffer[120 * bytesPerRow/4 + 160]
         CVPixelBufferUnlockBaseAddress(depthData.depthDataMap, .readOnly)
         print("Depth value at [160, 120] = \(px)")*/
        
        //preview.pixelBuffer = depthData.depthDataMap
        
    }
    
    func processDepth(depthData: AVDepthData){
        //print("Submitting frame #\(frameCount)...")
        
        xestoFramework.submitDepthFrame(depthData: depthData, frameNum: frameIndex)
        
        if (captureEnabled){
            
            if !videoDepthConverter.isPrepared {
                /*
                 outputRetainedBufferCountHint is the number of pixel buffers we expect to hold on to from the renderer. This value informs the renderer
                 how to size its buffer pool and how many pixel buffers to preallocate. Allow 2 frames of latency to cover the dispatch_async call.
                 */
                var depthFormatDescription: CMFormatDescription?
                CMVideoFormatDescriptionCreateForImageBuffer(kCFAllocatorDefault, depthData.depthDataMap, &depthFormatDescription)
                videoDepthConverter.prepare(with: depthFormatDescription!, outputRetainedBufferCountHint: 2)
            }
            
            guard let depthPixelBuffer = videoDepthConverter.render(pixelBuffer: depthData.depthDataMap) else {
                print("Unable to process depth")
                return
            }
            
            guard let bmpData = ViewController.imageData(withPixelBuffer: depthPixelBuffer, attachments: nil, format: kUTTypeBMP) else {
                print("Unable to create BMP photo")
                return
            }
            
            let res = self.writeFrame(data: bmpData, frameType: "depth", fileType: ".bmp")
            if res{
                print("Depth frame \(frameIndex) saved sucessfully!")
            }
            
            
            
            /*
            // Save BMP to photo library
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    PHPhotoLibrary.shared().performChanges({
                        let creationRequest = PHAssetCreationRequest.forAsset()
                        creationRequest.addResource(with: .photo, data: bmpData, options: nil)
                    }, completionHandler: { _, error in
                        if let error = error {
                            print("Error occurred while saving photo to photo library: \(error)")
                        }
                    })
                }
            }
            print("Depth Data saved as a bitmap")
            */
        }
    }
    
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        processVideo(sampleBuffer: sampleBuffer)
    }
    
    func processVideo(sampleBuffer: CMSampleBuffer){
        if (captureEnabled){
            guard let videoPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer),
                let formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer) else {
                    return
            }
            
            let width, height: CGFloat
            if (self.videoDeviceInput.device.deviceType == .builtInTrueDepthCamera){
                width = 640.0
                height = 480.0
            }
            else{
                width = 320.0
                height = 240.0
            }
            
            let resizedPixelBuffer = self.scalePixelBuffer(pixelBuffer: videoPixelBuffer, newWidth: width, newHeight: height)
            
            guard let bmpData = ViewController.imageData(withPixelBuffer: resizedPixelBuffer, attachments: nil, format: kUTTypeBMP) else {
                print("Unable to create BMP photo")
                return
            }
            
            let res = self.writeFrame(data: bmpData, frameType: "video", fileType: ".bmp")
            if res{
                print("Video frame \(frameIndex) saved sucessfully!")
            }
            
            /*
            // Save BMP to photo library
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    PHPhotoLibrary.shared().performChanges({
                        let creationRequest = PHAssetCreationRequest.forAsset()
                        creationRequest.addResource(with: .photo, data: bmpData, options: nil)
                    }, completionHandler: { _, error in
                        if let error = error {
                            print("Error occurred while saving photo to photo library: \(error)")
                        }
                    })
                }
            }
            print("Video Data saved as a bitmap")
             */
        }
    }
    
    
    func dataOutputSynchronizer(_ synchronizer: AVCaptureDataOutputSynchronizer, didOutput synchronizedDataCollection: AVCaptureSynchronizedDataCollection) {
        
        if let syncedDepthData: AVCaptureSynchronizedDepthData = synchronizedDataCollection.synchronizedData(for: depthDataOutput) as? AVCaptureSynchronizedDepthData {
            if !syncedDepthData.depthDataWasDropped {
                let depthData = syncedDepthData.depthData
                processDepth(depthData: depthData)
            }
        }
        
        if let syncedVideoData: AVCaptureSynchronizedSampleBufferData = synchronizedDataCollection.synchronizedData(for: videoDataOutput) as? AVCaptureSynchronizedSampleBufferData {
            if !syncedVideoData.sampleBufferWasDropped {
                let videoSampleBuffer = syncedVideoData.sampleBuffer
                processVideo(sampleBuffer: videoSampleBuffer)
            }
        }
        //Increment frame index
        frameIndex += 1
    }
    
    
    // MARK: - Utilities
    
    private class func imageData(withPixelBuffer pixelBuffer: CVPixelBuffer, attachments: CFDictionary?, format: CFString) -> Data? {
        
        var options : [String : Any]?
        
        let ciContext = CIContext()
        let renderedCIImage = CIImage(cvImageBuffer: pixelBuffer, options: options)
        guard let renderedCGImage = ciContext.createCGImage(renderedCIImage, from: renderedCIImage.extent) else {
            print("Failed to create CGImage")
            return nil
        }
        
        guard let data = CFDataCreateMutable(kCFAllocatorDefault, 0) else {
            print("Create CFData error!")
            return nil
        }
        
        guard let cgImageDestination = CGImageDestinationCreateWithData(data, format, 1, nil) else {
            print("Create CGImageDestination error!")
            return nil
        }
        
        CGImageDestinationAddImage(cgImageDestination, renderedCGImage, attachments)
        if CGImageDestinationFinalize(cgImageDestination) {
            return data as Data
        }
        print("Finalizing CGImageDestination error!")
        return nil
    }
    
    
    //Converts a 4 char code (e.g. an OSType) to a string
    func str4 (n: UInt32) -> String
    {
        var s: String = ""
        var i: UInt32 = n
        
        for _ in 0...3
        {
            s = String(UnicodeScalar(i & 255)!) + s
            i = i / 256
        }
        
        return (s)
    }
    
    //Rescales a pixel buffer to the specified dimensions.
    func scalePixelBuffer (pixelBuffer:CVImageBuffer, newWidth: CGFloat, newHeight: CGFloat) -> CVImageBuffer {
        let width = CGFloat( CVPixelBufferGetWidth(pixelBuffer) )
        let height = CGFloat( CVPixelBufferGetHeight(pixelBuffer) )
        let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
        let resizedCIImage = ciImage.transformed(by: CGAffineTransform(scaleX: newWidth / width, y: newHeight / height))
        let context = CIContext()
        var resizedPixelBuffer : CVPixelBuffer? = nil
        CVPixelBufferCreate(nil, Int(newWidth), Int(newHeight), CVPixelBufferGetPixelFormatType(pixelBuffer), nil, &resizedPixelBuffer)
        context.render(resizedCIImage, to: resizedPixelBuffer!)
        return resizedPixelBuffer!
    }
    
    //Constructs a file name and saves a frame inside the documents directory
    func writeFrame(data:Data, frameType: String, fileType:String) -> Bool{
        //Name and save file to documents directory
        if docDirectory != nil {
            var file = filePrefix + "_" + frameIndex.description + "_" + frameType
            if appendTimestamp{
                file += "_" + CACurrentMediaTime().description
            }
            file += fileType
            
            let fileURL = docDirectory!.appendingPathComponent(file)
            
            do{
                if captureEnabled{
                    try data.write(to: fileURL)
                }
                else{
                    print("Capture disabled. Aborting file write operation.")
                    return false
                }
            }
            catch{
                print("Unable to write file.")
                return false
            }
            return true
        }
        else{
            print("Error: unable to find documents directory.")
            return false
        }
    }
    
    //Sets an AVCaptureDevice to use a certain depth format. Inthis case it assumes the last item in supported depth formats will be max resoluton fdep
    func setCameraDepthFormat(videoDevice: AVCaptureDevice){
        let depthFormats = videoDevice.activeFormat.supportedDepthDataFormats
        //print(depthFormats)
        /*guard let fdep = depthFormats.first(where: $0) else {
         print("Camera does not support the desired depth format.")
         return
         }*/
        do {
            try videoDevice.lockForConfiguration()
            let fdep = depthFormats[depthFormats.count-1]
            videoDevice.activeDepthDataFormat = fdep
            videoDevice.unlockForConfiguration()
        }
        catch{
            print("Error setting depth format")
            return
        }
    }
    
    //Converts from a pose to a point on the screen
    func projectPoseOnScreen(pose: Pose) -> CGPoint {
        let previewBounds = previewLayer.bounds
        let screenBounds = UIScreen.main.bounds
        let scaleX  = previewBounds.size.width / screenBounds.size.width
        let scaleY  = previewBounds.size.height / screenBounds.size.height
        let rescaledWidth = (screenBounds.size.height * (4.0/3.0) )
        let centerPoint = CGPoint(x: screenBounds.size.width / 2, y: screenBounds.size.height / 2)
        let depthFocalLengthPix = ((rescaledWidth) / 2) / CGFloat(tan(57.6 * (Double.pi / 180) / 2))
        let relativePixelX = (CGFloat(pose.position.0) / (CGFloat(pose.position.2) / depthFocalLengthPix)) * scaleX
        let relativePixelY = (CGFloat(pose.position.1) / (CGFloat(pose.position.2) / depthFocalLengthPix)) * scaleY
        if pose.position.0.isFinite && pose.position.1.isFinite{
            return CGPoint(x: centerPoint.x + relativePixelX, y: centerPoint.y + relativePixelY)
        }
        else{
            return centerPoint
        }
        
    }

    

}

