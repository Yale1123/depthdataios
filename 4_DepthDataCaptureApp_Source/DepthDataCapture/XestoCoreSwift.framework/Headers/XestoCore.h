//
//  IXestoCore.h
//  XestoCoreLibrary
//
//  Created by SIRT Centre on 2018-03-02.

//

#ifndef XestoCore_h
#define XestoCore_h

#include "XestoTypes.h"

/*
 =========================
 Input and Configuration
 =========================
 */

///@brief   Initialize the Xesto Gesture SDK for use
///@param   docPath Absolute path to the directory that documents should be read from and written to
///@returns A value
XestoResult Init(const char * docPath);

XestoResult ConfigureDepth(DepthFormatType format, int width, int height);
XestoResult SubmitDepthFrame(int frameID, void * data);
XestoResult Shutdown();

/*
 =========================
Data Recording
 =========================
 */
XestoResult BeginRecordingAGesture();
XestoResult StopRecording();
XestoResult SaveRecordedGesture(long long gestureID);
XestoResult SaveGestureSetsToFile();

///@brief   Start writing out depth frames to the local file system
///@param filePrefix: Prefix to use for each individual depth frame file.  All files will be written to the document directory provided at initialization time
XestoResult StartDepthRecording(const char * filePrefix);
///@brief   Stop writing out depth frames to the local file system
XestoResult StopDepthRecording();



/*
 =========================
 Output
 =========================
 */

//Note: pointer parameters here are intended to be out parameters. On the swift side, they must be passed by reference.

int GetNumGestures();
int GetLastFrameID();
bool GetLastHandPose(HandPose * handPose);
bool GetLastObservedGesture(GestureObservation * gestureOb);
bool GetLastGestureConfidence(); //redundant
XestoResult GetFrameHandPose(int frameID, HandPose * handPose);
XestoResult GetFrameGestureConfidence(int frameID, long long gestureID, GestureObservation * gestureOb); //Possibly redundant


#endif /* XestoCore_h */
