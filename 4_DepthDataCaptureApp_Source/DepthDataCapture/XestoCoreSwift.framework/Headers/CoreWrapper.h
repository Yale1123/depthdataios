//
//  CoreWrapper.h
//  XestoCoreSwift
//

#ifndef CoreWrapper_h
#define CoreWrapper_h

#import <Foundation/Foundation.h>
#import "XestoTypes.h"

/**
 Provides Objective-C++ wrapper for the Xesto Core Libary
 */
@interface CoreWrapper: NSObject

/*
 =========================
 Input and Configuration
 =========================
 */

+ (XestoResult) coreInit;
+ (XestoResult) configureDepth:(DepthFormatType)format withWidth:(int)width withHeight:(int)height;
+ (XestoResult) submitDepthFrame:(int)frameID withData:(void *)data;
+ (XestoResult) shutdown;
+ (XestoResult) beginRecordingAGesture;
+ (XestoResult) stopRecording;
+ (XestoResult) saveRecordedGesture:(long long)gestureID;
+ (XestoResult) saveGestureSetsToFile;

+ (XestoResult) startDepthRecording: (const char *) filePrefix;
+ (XestoResult) stopDepthRecording;

/*
 =========================
 Output
 =========================
 */

+ (int) getNumGestures;
+ (int) getLastFrameID;
+ (bool) getLastHandPose:(HandPose *)handPose;
+ (bool) getLastObservedGesture:(GestureObservation *) gestureOb;
//+ (bool) getLastGestureConfidence;
+ (XestoResult) getFrameHandPose:(int)frameID out:(HandPose *)handPose;
+ (XestoResult) getFrameGestureConfidence:(int)frameID withGestureID:(long long)gestureID out:(GestureObservation *) gestureOb;

@end


#endif /* CoreWrapper_h */
