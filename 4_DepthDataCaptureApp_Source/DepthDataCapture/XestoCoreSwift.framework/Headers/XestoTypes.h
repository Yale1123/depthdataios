//
//  XestoTypes.h
//  XestoCoreLibrary
//
//  Created by SIRT Centre on 2018-03-02.

//

#ifndef XestoTypes_h
#define XestoTypes_h


/*
 =========================
 Enumerations
 =========================
 */

typedef enum {
    XestoResult_Success = 0,
    XestoResult_Failed = 1,
    XestoResult_NotImplemented = 2,
    XestoResult_NotInitialized = 3,
    XestoResult_InvalidArguments = 4
} XestoResult;


typedef enum {
    Depth_None,
    DepthFloat16,
    DepthFloat32,
    DisparityFloat16,
    DisparityFloat32
} DepthFormatType;


/*
 =========================
 Pose Information
 =========================
 */

typedef struct {
    float position[3];
    //float orientation[4];
} Pose;


//redundant
typedef struct {
    Pose palmPose;
    Pose fingerPoses[5];
} HandPose;


typedef struct {
    int FrameID;
    double timestamp;
    Pose palmPose;
    Pose fingerPoses[5];
} HandData;


/*
 =========================
 Gesture Information
 =========================
 */

//typedef struct {
//
//} GestureID;


typedef struct {
    long long gestureID;
    int frameStartID;
    int frameEndID;
    double timestamp;
    float confidence;
} GestureObservation;

#endif /* XestoTypes_h */
